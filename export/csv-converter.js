'use strict';

const json2csv = require('json2csv');

module.exports = class CSVConverter {
    constructor(options = {}) {
        this.options = options;
    }

    dataToCSV(data) {
        const result = json2csv.parse(data, this.options);
        return result;
    }
};
