'use strict';

const _ = require('lodash');

const shared = require('./shared.js');

exports.createLanguage = function createLanguage(req, res) {
    req.models.language.create(req.body)
        .then((result) => res.status(201).json(result))
        .catch(shared.handleError(req, res));
};

exports.getLanguage = function getLanguage(req, res) {
    const code = _.get(req, 'swagger.params.code.value');
    req.models.language.get(code)
        .then((result) => res.status(200).json(result))
        .catch(shared.handleError(req, res));
};

exports.patchLanguage = function patchLanguage(req, res) {
    const code = _.get(req, 'swagger.params.code.value');
    req.models.language.patch(code, req.body)
        .then(() => res.status(204).end())
        .catch(shared.handleError(req, res));
};

exports.deleteLanguage = function deleteLanguage(req, res) {
    const code = _.get(req, 'swagger.params.code.value');
    req.models.language.delete(code)
        .then(() => res.status(204).end())
        .catch(shared.handleError(req, res));
};

exports.listLanguages = function listLanguages(req, res) {
    req.models.language.list()
        .then((result) => res.status(200).json(result))
        .catch(shared.handleError(req, res));
};
