'use strict';

module.exports = {
    extends: '../.eslintrc.js',
    rules: {
        'import/no-extraneous-dependencies': [
            'error',
            { devDependencies: true },
        ],
        'max-classes-per-file': ['error', 4],
    },
};
