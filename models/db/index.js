'use strict';

const config = require('../../config');
const generator = require('./db-generator');

module.exports = generator(config.db);
