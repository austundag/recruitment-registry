'use strict';

const Sequelize = require('sequelize');
const pg = require('pg');

pg.types.setTypeParser(1184, (value) => value);

module.exports = function sequelizeGenerator(configdb) {
    const { name, user, pass } = configdb;
    const sequelize = new Sequelize(name, user, pass, configdb.options);
    return { Sequelize, sequelize };
};
