'use strict';

module.exports = function surveyStatus(sequelize, Sequelize, schema) {
    const tableName = 'survey_status';
    const modelName = `${schema}_${tableName}`;
    return sequelize.define(modelName, {
        name: {
            type: Sequelize.TEXT,
            allowNull: false,
            primaryKey: true,
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at',
        },
    }, {
        freezeTableName: true,
        tableName,
        schema,
        createdAt: 'createdAt',
        updatedAt: false,
        hooks: {
            afterSync(options) {
                if (options.force) {
                    const names = ['draft', 'published', 'retired'];
                    const ps = names.map((name) => this.create({ name }));
                    return Promise.all(ps);
                }
                return null;
            },
        },
    });
};
