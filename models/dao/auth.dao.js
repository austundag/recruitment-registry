'use strict';

const Sequelize = require('sequelize');
const RRError = require('../../lib/rr-error');

const { Base } = require('afssequelize');

const { Op } = Sequelize;

module.exports = class UserDAO extends Base {
    getUser({ id, username }) {
        const { User } = this.db;
        return User.findOne({
            raw: true,
            where: { id, originalUsername: username },
            attributes: ['id', 'username', 'email', 'role'],
        });
    }

    authenticateUser(username, password) {
        const { User } = this.db;
        const { sequelize } = this.db;
        return User.findOne({
            where: {
                [Op.or]: [
                    { username },
                    {
                        [Op.and]: [{
                            username: sequelize.fn('lower', sequelize.col('email')),
                        }, {
                            username: sequelize.fn('lower', username),
                        }],
                    },
                ],
            },
        })
            .then((user) => {
                if (user) {
                    if (user.role === 'import') {
                        return RRError.reject('authenticationImportedUser');
                    }
                    return user.authenticate(password)
                        .then(() => ({
                            id: user.id,
                            originalUsername: user.originalUsername,
                        }));
                }
                return RRError.reject('authenticationError');
            });
    }
};
