'use strict';

const dbGenerator = require('./db/db-generator');
const daosGenerator = require('./dao/daos-generator');

module.exports = function generator(configdb) {
    const db = dbGenerator(configdb);
    return daosGenerator(db);
};
