'use strict';

const fs = require('fs');
const csvToJson = require('csvtojson');

module.exports = class CSVConverter {
    constructor(options) {
        this.options = options || {};
    }

    streamToRecords(stream) {
        const converter = new csvToJson.Converter(this.options);
        return converter.fromStream(stream);
    }

    fileToRecords(filepath) {
        const stream = fs.createReadStream(filepath);
        return this.streamToRecords(stream);
    }
};
