'use strict';

const winston = require('winston');

const config = require('./config');

module.exports = winston.createLogger({
    transports: [
        new winston.transports.Console(),
    ],
    level: config.logging.level,
    format: winston.format.json(),
});
