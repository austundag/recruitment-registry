'use strict';

module.exports = {
    db: {
        options: {
            dialectOptions: {
                ssl: true,
            },
        },
    },
};
