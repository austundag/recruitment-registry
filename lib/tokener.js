'use strict';

const util = require('util');
const jwt = require('jsonwebtoken');

const config = require('../config');

const proxy = (token, callback) => {
    jwt.verify(token, config.jwt.secret, {}, callback);
};

exports.createJWT = function createJWT({ id, originalUsername }) {
    const { secret } = config.jwt;
    return jwt.sign({ id, username: originalUsername }, secret, { expiresIn: '30d' });
};

exports.verifyJWT = util.promisify(proxy, { context: jwt });
